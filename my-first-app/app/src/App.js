import React, { Component } from 'react';
import './App.css';
import axios from 'axios';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {lists: []}
    this.input1 = React.createRef()
  }

  addShoppingList = async () => {
    let res = await axios.post('http://servernode:4444/shopping-list', {name:this.input1.current.value});
    this.setState(state => ({
      lists: [...state.lists, {name:this.input1.current.value, id: res.data}]
    }))
  }

  async componentDidMount() {
    let res = await axios.get('http://servernode:4444/shopping-list');
    let data = res.data;
    console.log(data);
    this.setState({lists:data});       
  }


  render() {
    return (
      <div className="App">
        <form onSubmit={this.addShoppingList}>
          <input ref={this.input1} />
          <button type='submit'>add</button>
        </form>
        <p>
          {this.state.lists.map(e => <li key={e.id} >{e.name} </li>)}
        </p>
      </div>
    );
  }
}

export default App;

